import { Pipe, PipeTransform } from '@angular/core';
import { Heroe, Color } from '../interfaces/ventas.interfaces';

@Pipe({
  name: 'ordenar'
})
export class OrdenarPipe implements PipeTransform {

  transform(arreglo: Heroe[], ordenarPor: string = ''): Heroe[] {
    console.log(arreglo);

    switch (ordenarPor) {
      case 'nombre':
         return arreglo.sort((a,b) => (a.nombre > b.nombre) ? 1 : -1);
      case 'vuela':
         return arreglo.sort((a,b) => (a.vuela > b.vuela) ? -1 : 1);
      case 'color':
        return arreglo.sort((a,b) => (a.color > b.color) ? 1 : -1);
      default:
        return arreglo;
    }    
   
  }

}
