import { Component } from '@angular/core';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent  {
  
  nombreLower: string = 'triple h';
  nombreUpper: string = 'TRIPLE H';
  nombreCompleto: string = 'pAul LEVesQuE'

  fecha: Date = new Date();


}
