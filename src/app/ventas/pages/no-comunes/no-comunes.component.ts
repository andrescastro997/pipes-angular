import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  selector: 'app-no-comunes',
  templateUrl: './no-comunes.component.html',
  styles: [
  ]
})
export class NoComunesComponent implements OnInit {
  nombre: string = 'Triple H';
  genero: string = 'masculino'
  invitacionMapa = {
    'masculino' : 'invitarlo',
    'femenino' : 'invitarla'
  }

  clientes: string [] = ['X-pac', 'Mr.ass','Road Dogg', 'HBK', 'Hunter'];
  clientesMapa = {
    '=0': 'no tenemos ningun cliente esperando.',
    '=1': 'tenemos un cliente esperando',
    '=2': 'tenemos 2 clientes esperando',
    'other' : 'tenemos # clientes esperando'
  }
  constructor() { }

  ngOnInit(): void {
    
  }

  cambiarNombre(){
    if(this.genero === 'masculino'){
      this.nombre = 'Chyna';
      this.genero = 'femenino';
    }else{
      this.nombre = 'Triple H';
      this.genero = 'masculino'
    }
  }

  eliminarCliente(){
    this.clientes.pop();
  }

  //keyvalue pipe
  persona = {
    nombre: 'triple h',
    edad: '55',
    marca: 'raw'
  }

  //jsonPipe
  heroes = [
    {nombre: 'Spiderman', vuela: true},
    {nombre: 'Robin', vuela: false},
    {nombre: 'Aquaman', vuela: false}
  ]
  //async pipe
  miObservable = interval(5000);

  valorPromesa = new Promise((resolve, reject)=>{
    setTimeout(() =>{
      resolve('Tenemos data de promesa')
    }, 3500)
  });
}
