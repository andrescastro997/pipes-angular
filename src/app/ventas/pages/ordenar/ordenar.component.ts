import { Component, OnInit } from '@angular/core';
import { Color, Heroe } from '../../interfaces/ventas.interfaces';

@Component({
  selector: 'app-ordenar',
  templateUrl: './ordenar.component.html',
  styles: [
  ]
})
export class OrdenarComponent {
  mayusculasFlag: boolean = true;
  ordenarPor: string = '';
  heroes: Heroe[] = [
    {
      nombre: 'Superman',
      color: Color.azul,
      vuela: true
    },
    {
      nombre: 'Batman',
      color: Color.negro,
      vuela: false
    },
    {
      nombre: 'Linterna verde',
      color: Color.verde,
      vuela: true
    },
    {
      nombre: 'Spiderman',
      color: Color.rojo,
      vuela: false
    }
  ]
  toggleMayus() {
    this.mayusculasFlag ? this.mayusculasFlag = false : this.mayusculasFlag = true
  }

  cambiarOrden(valor: string){
    this.ordenarPor = valor;
    console.log(valor);
  }

}
