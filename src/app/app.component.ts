import { Component } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nombre: string = 'triple h';
  valor: number = 1000;
  obj = {
    nombre: 'triple h'
  }

  constructor(private primengConfig: PrimeNGConfig){}

  mostrarNombre(){
    console.log(this.nombre, this.valor, this.obj);
  }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
  }
}
